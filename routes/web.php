<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/projets', 'ProjetsController@index');
Route::get('/stage','StageController@index');
Route::get('/competences','CompetencesController@index');
Route::get('/veille','VeilleTechnologique@index');
Route::resource('/accueil','HomeController');
Route::resource('/projets','ProjetsController');
Route::resource('/stage','StageController');
Route::resource('/competences','CompetencesController');
Route::resource('/veille','VeilleTechnologique');

@extends('layout')
@section('photoswipe')
    <link rel="stylesheet" href="/css/photoswipe.css">
    <script src="/js/photoswipe.min.js"></script>
@section('title', 'Compétences')

@section('content')
    
    <div id="workwrap">
        <div class="container">
            <h1>COMPETENCES BTS SIO</h1>
        </div>
        <!-- /container -->
    </div>


    <div class="container centered mt mb">
        <h3 align="center">Liste des compétences :</h3>

        <table id="competences" class="table">
            <tr class="text-white">
                <td>Libelle</td>
                <td>Preuve</td>
                <td>Photo</td>
            </tr>
            @foreach ($competences as $competence)
                <tr class="text-white">
                    <td>{{ $competence->Libelle }}</td>
                    <td>{{ $competence->Preuve }}</td>
                    <td>
                        @if (!empty($competence->Photo))                                                 
                                <img class="img-responsive img-competence"
                                 src="{{ url('/image/preuve/' . $competence->Photo) }}"
                                 alt="image">                                                
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

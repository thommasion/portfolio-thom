@extends('layout')

@section('title', 'Accueil')

@section('content')
  <div id="headerwrap">
      <div class="container">
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <h1>THOM TEULIERE</h1>
          <h4>Développeur de sites et d'application Web</h4>         
        </div>
      </div>
    </div>
    <!-- /container -->
  </div>

    <div class="container">
        <ul id="passions" class="list-inline text-center">
            <li>
                <img class="img-responsive" src="/image/photo.jpg" alt="image">
            </li>
        </ul>
        <h3 align="center">A propos de moi</h3>
        <br>
        <p>Je m'appelle Thom TEULIERE, j'ai 20 ans, actuellement en 2ème année de BTS SIO au lycée Mermoz à
            Montpellier pour devenir développeur Web.</p>
        <p>Depuis toujours, j'ai été passionné par l'informatique et les coulisses concernant le Web.C'est pour
            celà que je me suis orienté dans l'univers de l'informatique.</p>
        <br>
        <h3 align="center">Mon parcours </h3><br>
        <p>Ayant obtenu mon bac pro SEN spécialité Télécommunications et Réseau avec mention bien en 2017,j'ai décidé de
            prendre une année de remise à
            niveau dans les matières scientifiques à la faculté des sciences de Montpellier.</p>
        <p>Suite à celà, J'ai décidé de poursuivre mes études en BTS SIO, une formation qui me
            correspond, alliant à la fois la programmation mais aussi le réseau.</p>
        <p>Suite au BTS, j'aimerais effectuer une licence professionnelle en alternance à l'IUT de
            Montpellier.</p>
        <br>
        <h3 align="center">Mes passions</h3><br>
        <ul id="passions" class="list-inline text-center">
            <li>
                <img class="img-responsive" src="/image/logo_jeux.png" alt="image">
            </li>
            <li>
                <img class="img-responsive" src="/image/logo_guitare.jpg" alt="image">
            </li>
            <li>
                <img class="img-responsive" src="/image/logo_cinema.jpg" alt="image">
            </li>
            <li>
                <img class="img-responsive" src="/image/logo_fff.png" alt="image">
            </li>
            <li>
                <img class="img-responsive" src="/image/logo_musique.jpg" alt="image">
            </li>
        </ul>
    </div>

  @endsection

@extends('layout')

@section('title', 'Projet')

@section('content')

    <div id="workwrap">
        <div class="container">
            <h1 class="m-auto">MES PROJETS</h1>
        </div>
        <!-- /container -->
    </div>

    <div class="works">
        <div class="centered mt mb">
            <h1>Application de gestion de cimetière</h1>
            <p>Cette application crée en mai 2019 lors de mon stage à la mairie de Saint Georges d'Orques permet de
                gérer le cimetière du village.</p>
            <p> Elle gère les concessionnaires, les concessions, les défunts et les ayant droits de la concession.</p>
            <p> Voici un aperçu de l'application : </p>
            <div id="projets">
            <img class="img-responsive mt m-auto" src="/image/Appli_Cimetiere.jpg">
            
            </div>
        </div>

        <div class="centered mt mb">
            <h1>Site de boutique en ligne</h1>
            <p>Projet réalisé dans le cadre de mes études, ce site de boutique en ligne comporte un back office et un
                front office.</p>
            <p>Le front office permet de créer un compte client,se connecter, d'ajouter des produits au panier, de
                valider le panier et de consulter le panier</p>
            <p> Le back office permet de gérer les catégories des produits, les produits et les clients. </p>
            <p> Voici un aperçu de l'application : </p>
        </div>
        <div class="centered mt mb">
            
            <p> Le front office : </p>
<div id="projets">
    <img class="img-responsive mt m-auto" src="/image/ProjetBoutique.jpg"><br>             
            <p> Le back office : </p>
            <img class="img-responsive mt m-auto" src="/image/back-office.jpg">
 </div>
    </div>
    </div>
@endsection

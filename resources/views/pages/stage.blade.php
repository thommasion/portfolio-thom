@extends('layout')

@section('title', 'Stage')

@section('content')

    <div id="workwrap">
        <div class="container">           
                    <h1>MES STAGES</h1>
                </div>
            </div>
        </div>
        <!-- /container -->
    </div>
    <section id="works"></section>
    <div class="align-right">
        <img src="/image/logo.jpg"></div>


    <div class="container">
        
        <h1 align="center">Mairie de Saint Georges d'Orques</h1>
        <h1 align="center">Du 13/05/19 au 21/06/19 et du 27/01/20 au 20/03/20</h1>
        <p>J'ai effectué mes deux stages de BTS SIO à la mairie se situant dans le village de Saint Georges
            d'Orques.</p>
        <p>L'objectif de ces deux stages étaient de développer une application web permettant de gérer le cimetière du
            village.</p>
        <br>      
        <img class="img-responsive m-auto" src="/image/Appli_Cimetiere.jpg">
    </div>
    <br>
    <div class="presentation">
        <a href="/diaporama/Présentation.odp" class="btn btn-default">Présentation du stage</a>
    </div>

@endsection




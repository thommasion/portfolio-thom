@extends('layout')

@section('title', 'Projet')

@section('content')

    <div id="workwrap">
        <div class="container">
            <h1 class="m-auto">VEILLE TECHNOLOGIQUE</h1>
        </div>
        <!-- /container -->
    </div>

    <div class="works">
        <div class="centered mt mb">
            <h1>LARAVEL</h1>
            <p>Laravel est un framework PHP qui respecte le modèle MVC(Modèle-Vue-Contrôleur)et entièrement
            codé en POO(Programmation Orienté Objet)</p>           
            <p>Veuillez-trouver ci contre les sites utilisé pour mener cette veille technologique : </p>
            <ul id="veille" class="list-inline text-center">
                <li>
                <a href="https://laravel.com/docs/7.x" target="_blank">
                   <img class="img-responsive mt m-auto" src="/image/Veille/Laravel.png">
                </a>
                </li>
                <li>
                <a href="https://openclassrooms.com/fr/courses/3613341-decouvrez-le-framework-php-laravel" target="_blank">
                   <img class="img-responsive mt m-auto" src="/image/Veille/OpenClassroom.jpg">
                </a>
                </li>
                <li>
                <a href="https://feedly.com/i/discover" target="_blank">
                   <img class="img-responsive mt m-auto" src="/image/Veille/feedly.png">
                </a>
                </li>
            </ul>
                
        </div>
      
        <div class="centered mt mb">
            
            <p> Le front office : </p>
<div id="projets">
    <img class="img-responsive mt m-auto" src="/image/ProjetBoutique.jpg"><br>             
            <p> Le back office : </p>
            <img class="img-responsive mt m-auto" src="/image/back-office.jpg">
 </div>
    </div>
    </div>
@endsection

 <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <a class="navbar-brand" href="{{route('accueil.index')}}">Portfolio</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-center">
          <li class="active"><a href="{{route('accueil.index')}}">Accueil</a></li>        
          <li><a href="{{route('projets.index')}}" class="smoothscroll">Projets</a></li>
          <li><a href="{{route('stage.index')}}" class="smoothscroll">Stages</a></li>
          <li><a href="{{route('competences.index')}}" class="smoothscroll">Compétences</a></li>
          <li><a href="{{route('veille.index')}}" class="smoothscroll">Veille Technologique</a></li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>




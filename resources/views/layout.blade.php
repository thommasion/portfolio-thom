<!DOCTYPE html>
<html lang="fr">
<head>
    @section('head')
        
        <meta charset="utf-8">
        <title>Portfolio Thom Teuliere - @yield('title')</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">
         
        <!-- Favicons -->
        <link href="/image/favicon.png" rel="icon">
        <link href="/image/apple-touch-icon.png" rel="apple-touch-icon">
        @yield('photoswipe')
    @show

    @section('css')
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
        <!-- Bootstrap CSS File -->
        <link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <!-- Main Stylesheet File -->
        <link href="/css/style.css" rel="stylesheet">

        <!-- =======================================================
          Template Name: Instant
          Template URL: https://templatemag.com/instant-bootstrap-personal-template/
          Author: TemplateMag.com
          License: https://templatemag.com/license/
        ======================================================= -->
    @show
</head>
<body>

@section('sidebar')
    @include('components/navbar')
@show

<div class="main">
    @yield('content')
</div>

</body>
</html>

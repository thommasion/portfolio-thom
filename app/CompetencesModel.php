<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetencesModel extends Model
{
     protected $primaryKey = 'idCompetence';
     protected $table = 'competences';
    
    
    protected $fillable = [
        'idCompetence', 'Libelle', 'Preuve','Photo'
    ];
}
